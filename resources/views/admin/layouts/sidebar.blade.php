<nav id="sidebar">
    <div class="sidebar-header">
        <h3><a href="/">{{ $settings->get('name') }}</a></h3>
    </div>

    <ul class="list-unstyled components">
        <li class="{{ Request::is('admin') ? 'active' : '' }}"><a href="{{ url('/admin') }}">@lang('titles.home')</a></li>
        <li class="{{ Request::is('admin/settings') ? 'active' : '' }}"><a href="{{ route('editSettings') }}">@lang('titles.settings')</a></li>
        <li class="{{ Request::is('admin/page') ? 'active' : '' }}"><a href="{{ route('page.index') }}">@lang('titles.page_management')</a></li>
        {{--<li class="{{ Request::is('admin/translations') ? 'active' : '' }}"><a href="{{ url('/translations') }}">@lang('titles.translations')</a></li>--}}
    </ul>

</nav>