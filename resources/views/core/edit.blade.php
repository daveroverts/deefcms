@extends('layouts.app')

@section('content')
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="card">
        <div class="card-header">@lang('titles.settings')</div>

        <div class="card-body">
            <form method="POST" action="{{route('editSettings')}}">
                @csrf
                {{--Name Website--}}
                <div class="form-group row">
                    <label for="name" class="col-md-4 col-form-label text-md-right">@lang('settings.core.name_site')*</label>

                    <div class="col-md-6">
                        <input id="name" type="text"
                               class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name"
                               value="{{ $settings->get('name') }}" data-toggle="tooltip" data-placement="top" title="@lang('settings.core.tooltip_name')" required autofocus>

                        @if($errors->has('name'))
                            <span class="invalid-feedback">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                        @endif
                    </div>
                </div>

                {{--Street--}}
                <div class="form-group row">
                    <label for="street" class="col-md-4 col-form-label text-md-right">@lang('settings.core.street')</label>

                    <div class="col-md-6">
                        <input id="street" type="text"
                               class="form-control{{ $errors->has('street') ? ' is-invalid' : '' }}"
                               name="street" value="{{ $settings->get('street') }}" data-toggle="tooltip" data-placement="top" title="@lang('settings.core.tooltip_street')">

                        @if($errors->has('street'))
                            <span class="invalid-feedback">
                                    <strong>{{ $errors->first('street') }}</strong>
                                </span>
                        @endif
                    </div>
                </div>

                {{--Postal code--}}
                <div class="form-group row">
                    <label for="postal_code" class="col-md-4 col-form-label text-md-right">@lang('settings.core.postal_code')</label>

                    <div class="col-md-6">
                        <input id="postal_code" type="text"
                               class="form-control{{ $errors->has('postal_code') ? ' is-invalid' : '' }}"
                               name="postal_code" value="{{ $settings->get('postal_code') }}" data-toggle="tooltip" data-placement="top" title="@lang('settings.core.tooltip_postal_code')">

                        @if($errors->has('postal_code'))
                            <span class="invalid-feedback">
                                    <strong>{{ $errors->first('postal_code') }}</strong>
                                </span>
                        @endif
                    </div>
                </div>

                {{--City--}}
                <div class="form-group row">
                    <label for="city" class="col-md-4 col-form-label text-md-right">@lang('settings.core.city')</label>

                    <div class="col-md-6">
                        <input id="city" type="text"
                               class="form-control{{ $errors->has('city') ? ' is-invalid' : '' }}" name="city"
                               value="{{ $settings->get('city') }}" data-toggle="tooltip" data-placement="top" title="@lang('settings.core.tooltip_city')">

                        @if($errors->has('city'))
                            <span class="invalid-feedback">
                                    <strong>{{ $errors->first('city') }}</strong>
                                </span>
                        @endif
                    </div>
                </div>

                {{--Telephone Number--}}
                <div class="form-group row">
                    <label for="telephone_number" class="col-md-4 col-form-label text-md-right"><i
                                class="fas fa-phone"></i> @lang('settings.core.tel_nr')</label>

                    <div class="col-md-6">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">+</span>
                            </div>
                            <input id="telephone_number" type="text"
                                   class="form-control{{ $errors->has('telephone_number') ? ' is-invalid' : '' }}"
                                   name="telephone_number" value="{{ $settings->get('telephone_number') }}" data-toggle="tooltip" data-placement="top" title="@lang('settings.core.tooltip_phone')">
                        </div>

                        @if($errors->has('telephone_number'))
                            <span class="invalid-feedback">
                                    <strong>{{ $errors->first('telephone_number') }}</strong>
                                </span>
                        @endif
                    </div>
                </div>

                {{--Facebook Link--}}
                <div class="form-group row">
                    <label for="facebook_link" class="col-md-4 col-form-label text-md-right"><i
                                class="fab fa-facebook-f"></i> Facebook</label>

                    <div class="col-md-6">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">facebook.com/</span>
                            </div>
                            <input id="facebook_link" type="text"
                                   class="form-control{{ $errors->has('facebook_link') ? ' is-invalid' : '' }}"
                                   name="facebook_link" value="{{ $settings->get('facebook_link') }}" data-toggle="tooltip" data-placement="top" title="@lang('settings.core.tooltip_facebook')">
                        </div>

                        @if($errors->has('facebook_link'))
                            <span class="invalid-feedback">
                                    <strong>{{ $errors->first('facebook_link') }}</strong>
                                </span>
                        @endif
                    </div>
                </div>

                {{--Twitter Link--}}
                <div class="form-group row">
                    <label for="twitter_link" class="col-md-4 col-form-label text-md-right"><i
                                class="fab fa-twitter"></i> Twitter</label>

                    <div class="col-md-6">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">twitter.com/</span>
                            </div>
                            <input id="twitter_link" type="text"
                                   class="form-control{{ $errors->has('twitter_link') ? ' is-invalid' : '' }}"
                                   name="twitter_link" value="{{ $settings->get('twitter_link') }}" data-toggle="tooltip" data-placement="top" title="@lang('settings.core.tooltip_twitter')">
                        </div>

                        @if($errors->has('twitterLink'))
                            <span class="invalid-feedback">
                                    <strong>{{ $errors->first('twitter_link') }}</strong>
                                </span>
                        @endif
                    </div>
                </div>

                {{--Youtube Link--}}
                <div class="form-group row">
                    <label for="youtube_link" class="col-md-4 col-form-label text-md-right"><i
                                class="fab fa-youtube"></i> Youtube</label>

                    <div class="col-md-6">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">youtube.com/</span>
                            </div>
                            <input id="youtube_link" type="text"
                                   class="form-control{{ $errors->has('youtube_link') ? ' is-invalid' : '' }}"
                                   name="youtube_link" value="{{ $settings->get('youtube_link') }}" data-toggle="tooltip" data-placement="top" title="@lang('settings.core.tooltip_youtube')">
                        </div>

                        @if($errors->has('ytLink'))
                            <span class="invalid-feedback">
                                    <strong>{{ $errors->first('youtube_link') }}</strong>
                                </span>
                        @endif
                    </div>
                </div>

                {{--Google+ Link--}}
                <div class="form-group row">
                    <label for="google_link" class="col-md-4 col-form-label text-md-right"><i
                                class="fab fa-google-plus-g"></i> Google+</label>

                    <div class="col-md-6">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">plus.google.com/+</span>
                            </div>
                            <input id="google_link" type="text"
                                   class="form-control{{ $errors->has('google_link') ? ' is-invalid' : '' }}"
                                   name="google_link" value="{{ $settings->get('google_link') }}" data-toggle="tooltip" data-placement="top" title="@lang('settings.core.tooltip_google')">
                        </div>

                        @if($errors->has('google_link'))
                            <span class="invalid-feedback">
                                    <strong>{{ $errors->first('google_link') }}</strong>
                                </span>
                        @endif
                    </div>
                </div>

                {{-- Instagram Link --}}
                <div class="form-group row">
                    <label for="instagram_link" class="col-md-4 col-form-label text-md-right"><i
                                class="fab fa-instagram"></i> Instagram</label>

                    <div class="col-md-6">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">instagram.com/</span>
                            </div>
                            <input id="instagram_link" type="text"
                                   class="form-control{{ $errors->has('instagram_link') ? ' is-invalid' : '' }}"
                                   name="instagram_link" value="{{ $settings->get('instagram_link') }}" data-toggle="tooltip" data-placement="top" title="@lang('settings.core.tooltip_instagram')">
                        </div>

                        @if($errors->has('instagramLink'))
                            <span class="invalid-feedback">
                                    <strong>{{ $errors->first('instagram_link') }}</strong>
                                </span>
                        @endif
                    </div>
                </div>

                {{-- Pinterest Link --}}
                <div class="form-group row">
                    <label for="pinterest_link" class="col-md-4 col-form-label text-md-right"><i
                                class="fab fa-pinterest-p"></i> Pinterest</label>

                    <div class="col-md-6">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">pinterest.com/</span>
                            </div>
                            <input id="pinterest_link" type="text"
                                   class="form-control{{ $errors->has('pinterestLink') ? ' is-invalid' : '' }}"
                                   name="pinterest_link" value="{{ $settings->get('pinterest_link') }}" data-toggle="tooltip" data-placement="top" title="@lang('settings.core.tooltip_pinterest')">
                        </div>

                        @if($errors->has('pinterest_link'))
                            <span class="invalid-feedback">
                                    <strong>{{ $errors->first('pinterest_link') }}</strong>
                                </span>
                        @endif
                    </div>
                </div>

                {{--Submit--}}
                <div class="form-group row mb-0">
                    <div class="col-md-6 offset-md-4">
                        <button type="submit" class="btn btn-primary">@lang('general.save')</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

@endsection