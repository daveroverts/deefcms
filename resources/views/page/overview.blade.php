@extends('layouts.app')

@section('content')

                <h2>@lang('titles.all_pages')</h2>
                <a href="{{ route('page.create') }}">
                <button class="btn" data-toggle="tooltip" data-placement="top" title="@lang('pages.tooltip_create_head') [level 1]">
                    <i class="fas fa-plus" style="color:green"></i> @lang('pages.create_new_page')
                </button>
                </a>
                <table class="table table-hover" style="margin-top: 10px;">
                    <tbody>
                    @php
                        $subPages = [];

                    @endphp
                    @forelse($relations as $relation)
                        {{-- Headpage that has no subpages [level 1] --}}
                        @if(empty($relation['subPageId']))
                            @php($page = $pages->where('id',$relation['id'])->first())
                            @include('page.overview.show',[$page, $sub = 0])
                        @else
                            {{-- Headpage that has subpages [level 1] --}}
                            @php(array_push($subPages, $relation['subPageId']))
                            @if(!in_array($relation['id'], $subPages))
                                @php($page = $pages->where('id',$relation['id'])->first())
                                @include('page.overview.show',[$page, $sub = 0])
                            @endif
                            {{-- Subpage that belongs to another subpage [level 3] --}}
                            @if(in_array($relation['id'], $subPages))
                                @php($page = $pages->where('id',$relation['subPageId'])->first())
                                @include('page.overview.show',[$page, $sub = 2])
                            @else
                                {{-- Subpage that belongs to a headpage [level 2] --}}
                                @php($page = $pages->where('id',$relation['subPageId'])->first())
                                @include('page.overview.show',[$page, $sub = 1])
                            @endif
                        @endif

                    @empty
                        <p>@lang('pages.no_pages')</p>
                    @endforelse
                    </tbody>
                </table>
@endsection