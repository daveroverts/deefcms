@extends('layouts.app')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">@lang('titles.create_page')</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('page.store') }}">
                        @csrf

                        <h3>@lang('pages.details')</h3>
                        {{-- Online --}}
                        <div class="form-group row">
                            <label for="online" class="col-md-4 col-form-label text-md-right">@lang('pages.online')*</label>

                            <div class="col-md-6">
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" name="isOnline" type="radio" id="isOnline_1" value="1" checked data-toggle="tooltip" data-placement="top" title="@lang('pages.tooltip_online')">
                                    <label class="form-check-label" for="isOnline_1">@lang('general.yes')</label>
                                </div>

                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" name="isOnline" type="radio" id="isOnline_0" value="0" data-toggle="tooltip" data-placement="top" title="@lang('pages.tooltip_offline')">
                                    <label class="form-check-label" for="isOnline_0">@lang('general.no')</label>
                                </div>
                            </div>
                        </div>

                        {{-- Title --}}
                        <div class="form-group row">
                            <label for="title" class="col-md-4 col-form-label text-md-right">@lang('pages.title')*</label>

                            <div class="col-md-6">
                                <input id="title" type="text"
                                       class="form-control{{ $errors->has('title') ? ' is-invalid' : '' }}" name="title" value="{{ old('title') }}" required autofocus data-toggle="tooltip" data-placement="top" title="@lang('pages.tooltip_title')">

                                @if($errors->has('title'))
                                    <span class="invalid-feedback">
                                    <strong>{{ $errors->first('title') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        {{-- Head Page --}}
                        @if($headPage)
                            <div class="form-group row">
                                <label for="headPage" class="col-md-4 col-form-label text-md-right">@lang('pages.head_page')</label>

                                <div class="col-md-6">
                                    <input name="headPage" type="hidden" value="{{ $headPage->id }}">
                                    <input id="headPageTitle" type="text" class="form-control{{ $errors->has('headPageTitle') ? ' is-invalid' : '' }}" name="headPageTitle" value="{{ $headPage->title }}" disabled  data-toggle="tooltip" data-placement="top" title="@lang('pages.tooltip_belongs_to_subpage')">
                                </div>
                            </div>
                        @endif

                        {{-- Content --}}
                        <div class="form-group row">
                            <textarea id="tinymce" name="tinymce"
                                      rows="10">{!! old(html_entity_decode('tinymce')) !!}</textarea>
                        </div>

                        {{--Submit--}}
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">@lang('general.save')</button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection