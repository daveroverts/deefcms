@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col">
            <h1>{{ $page->title }}</h1>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col">
            {!! html_entity_decode($page->content) !!}
        </div>
    </div>

@endsection

