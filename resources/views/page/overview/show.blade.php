<tr>
    <td>
        @if($sub < 2 && $page->allowSub)
            <a href="{{ route('page.create',$page->id) }}">
                <button class="btn" data-toggle="tooltip" data-placement="top" title="@lang('pages.tooltip_create_sub') [{{$sub == 1 ? 'level 3' : 'level 2'}}]">
                    <i class="fas fa-plus" style="color:green"></i>
                </button>
            </a>
        @endif
            @if($sub > 0)
                @if($sub == 2)
                    <button class="btn disabled" data-toggle="tooltip" data-placement="top" title="@lang('pages.tooltip_create_sub_unable')" disabled>
                        <i class="fas fa-ban"></i>
                    </button>
                @endif
                <button class="btn disabled" style="{{ $sub == 2 ? 'padding: 0.375rem 1.5rem;' : '' }}" disabled>
                    <i class="fas fa-level-up-alt fa-rotate-90"></i>&nbsp;
                </button>
            @endif
        <a href="{{ route('page.show', $page->slug) }}" data-toggle="tooltip" data-placement="top" title="@lang('pages.tooltip_open')">{{ $page->title }}</a></td>
    <td>
        <form action="{{ route('page.setOnline', [$page->id, $page->isOnline]) }}" method="GET">
            @csrf
            @if($page->isOnline)
                @if($page->allowOnline())
                    <button class="btn" data-toggle="tooltip" data-placement="top" title="@lang('pages.tooltip_offline')">
                        <i class="fas fa-toggle-on" style=" color:green"></i>
                    </button>
                @else
                    <button class="btn disabled" data-toggle="tooltip" data-placement="top" title="@lang('pages.tooltip_online_unable')" disabled>
                        <i class="fas fa-toggle-on" ></i>
                    </button>
                @endif
            @else
                @if($page->allowOnline() || Auth::check() && Auth::user()->isAdmin())
                    <button class="btn" data-toggle="tooltip" data-placement="top" title="@lang('pages.tooltip_online')">
                        <i class="fas fa-toggle-off" style="color:red"></i>
                    </button>
                @else
                    <button class="btn" data-toggle="tooltip" data-placement="top" title="@lang('pages.tooltip_online_unable')" disabled>
                        <i class="fas fa-toggle-off"></i>
                    </button>
                @endif
            @endif
        </form>
    </td>
    <td>
        <form action="{{ route('page.edit', $page->id) }}">
            @if($page->allowEdit() || Auth::check() && Auth::user()->isAdmin())
            @csrf
            <button type="submit" class="btn" data-toggle="tooltip" data-placement="top" title="@lang('pages.tooltip_edit')">
                <i class="far fa-edit"></i>
            </button>
            @else
                <button type="submit" class="btn disabled" data-toggle="tooltip" data-placement="top" title="@lang('pages.tooltip_edit_unable')" disabled>
                    <i class="far fa-edit"></i>
                </button>
            @endif
        </form>
    </td>
    <td>
        @if($page->allowDelete && !$page->hasSubPage())
            <form action="{{ route('page.destroy', $page->id) }}" method="POST">
                @method('DELETE')
                @csrf
                <button type="submit" class="btn"  data-toggle="tooltip" data-placement="top" title="@lang('pages.tooltip_delete')">
                    <i class="far fa-trash-alt" style="color:red"></i>
                </button>
            </form>
        @else
            <button type="submit" class="btn disabled" data-toggle="tooltip" data-placement="top" title="@lang('pages.tooltip_delete_unable')" disabled>
                <i class="far fa-trash-alt" style="color:gray"></i>
            </button>
        @endif

    </td>
</tr>