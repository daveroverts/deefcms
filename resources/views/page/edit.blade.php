@extends('layouts.app')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">@lang('titles.edit_page')</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('page.update', $page->id) }}">
                        @csrf
                        @method('PATCH')
                        <h3>@lang('pages.details')</h3>
                        @if($page->allowOnline())
                        {{-- Online --}}
                        <div class="form-group row">
                            <label for="online" class="col-md-4 col-form-label text-md-right">@lang('pages.online')*</label>

                            <div class="col-md-6">
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" name="isOnline" type="radio" id="isOnline_1" value="1" {{ $page->isOnline ? 'checked' : '' }}  data-toggle="tooltip" data-placement="top" title="@lang('pages.tooltip_online')">
                                    <label class="form-check-label" for="isOnline_1">@lang('general.yes')</label>
                                </div>

                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" name="isOnline" type="radio" id="isOnline_0" value="0" {{ !$page->isOnline ? 'checked' : '' }}  data-toggle="tooltip" data-placement="top" title="@lang('pages.tooltip_offline')">
                                    <label class="form-check-label" for="isOnline_0">@lang('general.no')</label>
                                </div>
                            </div>
                        </div>
                        @endif

                        {{-- Title --}}
                        <div class="form-group row">
                            <label for="title" class="col-md-4 col-form-label text-md-right">@lang('pages.title')*</label>

                            <div class="col-md-6">
                                <input id="title" type="text"
                                       class="form-control{{ $errors->has('title') ? ' is-invalid' : '' }}" name="title" value="{{ $page->title }}" required autofocus  data-toggle="tooltip" data-placement="top" title="@lang('pages.tooltip_title')">

                                @if($errors->has('title'))
                                    <span class="invalid-feedback">
                                    <strong>{{ $errors->first('title') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        {{-- Head Page --}}
                        @if($page->headPage)
                            <div class="form-group row">
                                <label for="headPage" class="col-md-4 col-form-label text-md-right">@lang('pages.head_page')</label>

                                <div class="col-md-6">
                                    <input name="headPage" type="hidden" value="{{ $page->headPage->id }}">
                                    <input id="headPageTitle" type="text" class="form-control{{ $errors->has('headPageTitle') ? ' is-invalid' : '' }}" name="headPageTitle" value="{{ $page->headPage->title }}" disabled  data-toggle="tooltip" data-placement="top" title="@lang('pages.tooltip_belongs_to_subpage')">
                                </div>
                            </div>
                        @endif

                        {{-- Content --}}
                        <div class="form-group row">
                            <textarea id="tinymce" name="tinymce">{!! html_entity_decode($page->content) !!}</textarea>
                        </div>

                        @if(Auth::check() && Auth::user()->isAdmin())
                            {{-- Radio's --}}

                            <h3>Admin settings</h3>
                            <div class="form-group row">
                                <div class="col-sm-3">
                                    <label for="currentURL" class="col-form-label">@lang('pages.current_url')</label>
                                </div>
                                <div class="col-sm-9">
                                    <div class="form-check form-check-inline">
                                        {{ route('page.show', $page->slug) }}
                                    </div>
                                </div>
                            </div>
                            {{-- Can change online --}}
                            <div class="form-group row">
                                <div class="col-sm-3">
                                    <label for="allowOnline" class="col-form-label">@lang('pages.online_changeable')</label>
                                </div>
                                <div class="col-sm-9">
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" id="allowOnlineYes" name="allowOnline" value="1" {{ $page->allowOnline ? 'checked' : '' }} data-toggle="tooltip" data-placement="top" title="@lang('pages.tooltip_online_changeable')">
                                        <label class="form-check-label" for="allowOnlineYes">@lang('general.yes')</label>
                                    </div>

                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" id="allowOnlineNo" name="allowOnline" value="0" {{ !$page->allowOnline ? 'checked' : '' }} data-toggle="tooltip" data-placement="top" title="@lang('pages.tooltip_online_changeable')">
                                        <label class="form-check-label" for="allowOnlineNo">@lang('general.no')</label>
                                    </div>
                                </div>
                            </div>

                            {{-- Can be edited by user --}}
                            {{-- Can change online --}}
                            <div class="form-group row">
                                <div class="col-sm-3">
                                    <label for="allowEdit" class="col-form-label">@lang('pages.editable')</label>
                                </div>
                                <div class="col-sm-9">
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" id="allowEditYes" name="allowEdit" value="1" {{ $page->allowEdit ? 'checked' : '' }} data-toggle="tooltip" data-placement="top" title="@lang('pages.tooltip_editable')">
                                        <label class="form-check-label" for="allowEditYes">@lang('general.yes')</label>
                                    </div>

                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" id="allowEditNo" name="allowEdit" value="0" {{ !$page->allowEdit ? 'checked' : '' }} data-toggle="tooltip" data-placement="top" title="@lang('pages.tooltip_editable')">
                                        <label class="form-check-label" for="allowEditNo">@lang('general.no')</label>
                                    </div>
                                </div>
                            </div>

                            {{-- Can be deleted by user --}}
                            <div class="form-group row">
                                <div class="col-sm-3">
                                    <label for="allowDelete" class="col-form-label">@lang('pages.deletable')</label>
                                </div>
                                <div class="col-sm-9">
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" id="allowDeleteYes" name="allowDelete" value="1" {{ $page->allowDelete ? 'checked' : '' }} data-toggle="tooltip" data-placement="top" title="@lang('pages.tooltip_deletable')">
                                        <label class="form-check-label" for="allowDeleteYes">@lang('general.yes')</label>
                                    </div>

                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" id="allowDeleteNo" name="allowDelete" value="0" {{ !$page->allowDelete ? 'checked' : '' }} data-toggle="tooltip" data-placement="top" title="@lang('pages.tooltip_deletable')">
                                        <label class="form-check-label" for="allowDeleteNo">@lang('general.no')</label>
                                    </div>
                                </div>
                            </div>

                            {{-- Can have sub-pages --}}
                            <div class="form-group row">
                                <div class="col-sm-3">
                                    <label for="allowSub" class="col-form-label">@lang('pages.may_have_sub')</label>
                                </div>
                                <div class="col-sm-9">
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" id="allowSubYes" name="allowSub" value="1" {{ $page->allowSub ? 'checked' : '' }} data-toggle="tooltip" data-placement="top" title="@lang('pages.tooltip_may_have_sub')">
                                        <label class="form-check-label" for="allowSubYes">@lang('general.yes')</label>
                                    </div>

                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" id="allowSubNo" name="allowSub" value="0" {{ !$page->allowSub ? 'checked' : '' }} data-toggle="tooltip" data-placement="top" title="@lang('pages.tooltip_may_have_sub')">
                                        <label class="form-check-label" for="allowSubNo">@lang('general.no')</label>
                                    </div>
                                </div>
                            </div>

                            {{-- Lock URL --}}
                            <div class="form-group row">
                                <div class="col-sm-3">
                                    <label for="allowLock" class="col-form-label">@lang('pages.lock_url_path')</label>
                                </div>
                                <div class="col-sm-9">
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" id="allowLockYes" name="allowLock" value="1" {{ $page->allowLock ? 'checked' : '' }} data-toggle="tooltip" data-placement="top" title="@lang('pages.tooltip_lock_url_path')">
                                        <label class="form-check-label" for="allowLockYes">@lang('general.yes')</label>
                                    </div>

                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" id="allowLockNo" name="allowLock" value="0" {{ !$page->allowLock ? 'checked' : '' }} data-toggle="tooltip" data-placement="top" title="@lang('pages.tooltip_lock_url_path')">
                                        <label class="form-check-label" for="allowLockNo">@lang('general.no')</label>
                                    </div>
                                </div>
                            </div>
                        @endif

                        {{--Submit--}}
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">@lang('general.save')</button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection