@extends('layouts.app')

@section('content')
    @php
        $settings = \App\Setting::all()->mapWithKeys(function ($item) {
                return [$item['name'] => $item['value']];
            });
    @endphp
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">@lang('titles.homepage')</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    @lang('general.welcome_to') {{ $settings->get('name') }}
                </div>
            </div>
        </div>
    </div>
@endsection
