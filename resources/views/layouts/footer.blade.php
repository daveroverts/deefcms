<footer><span class="fa fa-copyright"></span> <?php
    //An script to generate the copyright date using the server's year
    $fromYear = 2018;
    $thisYear = (int)date('Y');
    echo $fromYear . (($fromYear != $thisYear) ? '-' . $thisYear : '');?> Deef CMS, @lang('general.created_by') <a
            href="https://github.com/daveroverts/">Dave Roverts</a>.
</footer>