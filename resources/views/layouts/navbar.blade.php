@php
    $pages = \App\Page::all();
    function generateSocialIcon($url, $icon, $color) {
        return "<li class='nav-item'><a class='nav-link' href='".$url."'><i class='".$icon."' style='color:".$color.";'></i></a></li>";
        }
@endphp
<nav class="navbar navbar-expand-md navbar-light navbar-laravel">
    <div class="container">
        <a class="navbar-brand" href="{{ url('/') }}">
            {{ $settings->get('name') }}
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- Left Side Of Navbar -->
            <ul class="navbar-nav mr-auto">

            </ul>

            {{-- Pages --}}
            <ul class="navbar-nav ml-auto">
                @php
                    $subPages = [];
                    $alreadyIncluded = [];
                    $relations = (new App\Http\Controllers\PageController)->getPagesRelation();
                @endphp
                @foreach($relations as $relation)
                    {{-- Headpage that has no subpages [level 1] --}}
                    @if(empty($relation['subPageId']))
                        @php($page = $pages->where('id',$relation['id'])->first())
                        @if($page->isOnline)
                            <li class="nav-item {{ Request::is([$page->slug, 'page/'.$page->slug]) ? 'active' : '' }}"><a
                                        class="nav-link" href="{{ route('page.show', $page->slug) }}">{{ $page->title }}</a>
                            </li>
                        @endif
                    @else
                        {{-- Headpage that has subpages [level 1] --}}
                        @php(array_push($subPages, $relation['subPageId']))
                            @if(!in_array($relation['id'], $subPages))
                                @php($page = $pages->where('id',$relation['id'])->first())
                                @if($page->isOnline)
                                    <li class="nav-item  {{ Request::is([$page->slug, 'page/'.$page->slug]) ? 'active' : '' }} dropdown">
                                        <a id="navbarDropdownLeft" class="nav-link dropdown-toggle" href="{{ route('page.show', $page->slug) }}" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{ $page->title }}</a>
                                        <div class="dropdown-menu" aria-labelledby="navbarDropdownLeft">
                                            <a class="dropdown-item {{ Request::is([$page->slug, 'page/'.$page->slug]) ? 'active' : '' }}" href="{{ route('page.show', $page->slug) }}">{{ $page->title }}</a>
                                            {{-- Subpage that belongs to a headpage [level 2] --}}
                                            @php($page = $pages->where('id',$relation['subPageId'])->first())
                                            @if($page->isOnline)
                                                <a class="dropdown-item {{ Request::is([$page->slug, 'page/'.$page->slug]) ? 'active' : '' }}" href="{{ route('page.show', $page->slug) }}"><i class="fas fa-level-up-alt fa-rotate-90"></i>&nbsp;{{ $page->title }}</a>
                                                {{-- Subpage that belongs to another subpage [level 3] --}}
                                                @foreach($relations as $relation)
                                                    @if(in_array($relation['id'], $subPages))
                                                        @forelse($alreadyIncluded as $dontInclude => $value)
                                                            @if($relation['subPageId'] == $value)

                                                            @endif
                                                            @empty
                                                            @php($page = $pages->where('id',$relation['subPageId'])->first())
                                                            @if($page->isOnline)
                                                                @php($alreadyIncluded[] = $page->id)
                                                                <a class="dropdown-item {{ Request::is([$page->slug, 'page/'.$page->slug]) ? 'active' : '' }}" href="{{ route('page.show', $page->slug) }}"><i class="fas fa-level-up-alt fa-rotate-90" style="visibility: hidden;"></i>&nbsp;<i class="fas fa-level-up-alt fa-rotate-90"></i>&nbsp;{{ $page->title }}</a>
                                                            @endif
                                                        @endforelse
                                                    @endif
                                                @endforeach
                                            @endif
                                        </div>
                                    </li>
                                @endif
                            @endif
                    @endif
                @endforeach
            </ul>

            {{-- Middle Side of Navbar --}}
            <ul class="navbar-nav ml-auto">
                @if(null !== $settings->get('facebook_link'))
                    {!! generateSocialIcon(url('//facebook.com/'.$settings->get('facebook_link')),'fab fa-facebook fa-2x','#3b5998') !!}
                @endif

                @if(null !== $settings->get('twitter_link'))
                    {!! generateSocialIcon(url('//twitter.com/'.$settings->get('twitter_link')),'fab fa-twitter fa-2x','#55acee') !!}
                @endif

                @if(null !== $settings->get('youtube_link'))
                    {!! generateSocialIcon(url('//youtube.com/'.$settings->get('youtube_link')),'fab fa-youtube fa-2x','#e52d27') !!}
                @endif

                @if(null !== $settings->get('google_link'))
                    {!! generateSocialIcon(url('//plus.google.com/+'.$settings->get('google_link')),'fab fa-google-plus-g fa-2x','#e52d27') !!}
                @endif

                @if(null !== $settings->get('telephone_number'))
                    {!! generateSocialIcon(url('//tel:'.$settings->get('telephone_number')),'fas fa-phone fa-2x','') !!}
                @endif

                @if(null !== $settings->get('instagram_link'))
                    {!! generateSocialIcon(url('//instagram.com/'.$settings->get('instagram_link')),'fab fa-instagram fa-2x','#3f729b') !!}
                @endif

                @if(null !== $settings->get('pinterest_link'))
                    {!! generateSocialIcon(url('//pinterest.com/'.$settings->get('pinterest_link')),'fab fa-pinterest-p fa-2x','#cc2127') !!}
                @endif
            </ul>

            <!-- Right Side Of Navbar -->
            <ul class="navbar-nav ml-auto">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        @lang('languages.'. App::getLocale()) <span class="flag-icon flag-icon-{{ App::getLocale() == 'en' ? 'gb' : App::getLocale() }}"></span>
                    </a>
                    <ul class="dropdown-menu">
                        @foreach (Config::get('app.languages') as $language)
                            @if ($language != App::getLocale())
                                    <a class="dropdown-item" href="{{ route('langroute', $language) }}">@lang('languages.'. $language)
                                    @if($language == 'en' ? $language = 'gb' : '')@endif
                                        <span class="flag-icon flag-icon-{{ $language }}"></span>
                                    </a>
                            @endif
                        @endforeach
                    </ul>
                </li>
                <!-- Authentication Links -->
                @guest
                    <li><a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a></li>
                    {{--<li><a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a></li>--}}
                @else
                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>

                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                            @if(Auth::check() && Auth::user()->isContentCreator())
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item {{ Request::is('page') ? 'active' : '' }}"
                                   href="{{ route('page.index') }}">@lang('titles.all_pages')</a>
                                @if(Auth::user()->isAdmin())
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item {{ Request::is('edit/core') ? 'active' : '' }}"
                                       href="{{ route('editSettings') }}">@lang('titles.settings')</a>
                                @endif
                            @endif
                        </div>
                    </li>
                @endguest
            </ul>
        </div>
    </div>
</nav>