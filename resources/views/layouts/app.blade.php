<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
@php
    $settings = \App\Setting::all()->mapWithKeys(function ($item) {
        return [$item['name'] => $item['value']];
        });
@endphp
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ $settings->get('name') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/tinymce/tinymce.min.js') }}"></script>
    <script>tinymce.init({
            selector: 'textarea',
            plugins: 'code'
        });</script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
<div id="app">
    @if(Request::is('admin*'))
        @include('admin.layouts.sidebar')
        @else
        @include('layouts.navbar')
    @endif
        <main class="py-4">
            <div class="container">
                @if(Request::is('admin*'))
                    <div class="wrapper">
                        <div id="content">
                            <div class="row">
                                <div class="col">
                                    <button type="button" id="sidebarCollapse" class="btn btn-info navbar-btn">
                                        <i class="fas fa-bars"></i>
                                        Toggle Sidebar
                                    </button>
                                    <hr>
                @endif
                @yield('content')
                @if(Request::is('admin*'))
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
                @if(!Request::is('admin*'))
                    @include('layouts.footer')
                @endif
        </div>
    </main>
</div>
</body>
</html>
