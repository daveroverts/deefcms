<?php

return array (
  'home' => 'Home',
  'settings' => 'Settings',
  'page_management' => 'Page management',
  'all_pages' => 'All pages',
  'create_page' => 'Create page',
  'edit_page' => 'Edit page',
  'translations' => 'Translations',
  'homepage' => 'Homepage',
);
