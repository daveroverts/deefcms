<?php

return array (
  'core' => 
  array (
    'city' => 'City',
    'name_site' => 'Name website',
    'postal_code' => 'Postal code',
    'street' => 'Street',
    'tel_nr' => 'Telephone number',
    'tooltip_city' => 'Fill in city (not required)',
    'tooltip_facebook' => 'Fill in Facebook link. Will be shown in the navbar (not required)',
    'tooltip_google' => 'Fill in Google+ name. Will be shown in the navbar (not required)',
    'tooltip_instagram' => 'Fill in Instagram username. Will be shown in the navbar (not required)',
    'tooltip_name' => 'Fill in the name of the website. This will be shown on the left of the navbar (required)',
    'tooltip_phone' => 'Fill in the phone number. Will be shown in the navbar (not required)',
    'tooltip_pinterest' => 'Fill in Pinterest name. Will be shown in the navbar (not required)',
    'tooltip_postal_code' => 'Fill in Postal Code. At this time, it won\'t show up anywhere.',
    'tooltip_street' => 'Fill in Street, at this time, it won\'t show up anywhere.',
    'tooltip_twitter' => 'Fill in Twitter handler. Will be shown in the navbar (not required)',
    'tooltip_youtube' => 'Fill in Youtube channel. Include \'/user/\' or \'/channel/\' before the actual id. To find out what you need, goto your YouTube channel, and fill in what\'s in the adress bar.
Will be shown in the navbar (not required)',
  ),
);
