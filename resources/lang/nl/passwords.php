<?php

return array (
  'password' => 'U heeft geen veilig wachtwoord ingevuld (minimaal 6 tekens)',
  'reset' => 'Wachtwoord is opnieuw ingesteld!',
  'sent' => 'Er is een Email verstuurd met een wachtwoord reset link',
  'token' => 'Het wachtwoord reset token is niet geldig',
  'user' => 'Er is geen gebruiker gevonden met het ingevulde email adres',
);
