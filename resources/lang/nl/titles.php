<?php

return array (
  'home' => 'Home',
  'settings' => 'Instellingen',
  'page_management' => 'Pagina beheer',
  'all_pages' => 'Alle pagina\'s',
  'edit_page' => 'Pagina bijwerken',
  'create_page' => 'Pagina toevoegen',
  'translations' => 'Vertalingen',
  'homepage' => 'Homepagina',
);
