<?php

return array (
  'core' => 
  array (
    'name_site' => 'Naam website',
    'street' => 'Straat',
    'postal_code' => 'Postcode',
    'city' => 'Stad',
    'tel_nr' => 'Telefoon nummer',
    'tooltip_name' => 'Vul de naam van de website in. Wordt op de linkerkant van de navigatie bar laten zien.',
    'tooltip_street' => 'Vul een straat in. Wordt op dit moment nergens aangetoond.',
    'tooltip_postal_code' => 'Vul een postcode in. Wordt op dit moment nergens aangetoond.',
    'tooltip_city' => 'Vul een plaats in (niet verplicht)',
    'tooltip_phone' => 'Vul een telefoon nummer in. Wordt op de navigatie bar gezet (niet verplicht)',
    'tooltip_facebook' => 'Vul een Facebook link in. Wordt op de navigatie bar gezet (niet verplicht)',
    'tooltip_twitter' => 'Vul een Twitter handler in. Wordt op de navigatie bar gezet (niet verplicht)',
    'tooltip_youtube' => 'Vul een YouTube kanaal in. Voeg \'/user\' of \'/channel/\' toe voordat je de id toevoegt. Om uit te zoeken welke je nodig hebt, ga naar je YouTube kanaal en kijk wat op de adresbalk staat.
Wordt op de navigatie bar gezet (niet verplicht)',
    'tooltip_google' => 'Vul een Google+ naam in. Wordt op de navigatie bar gezet (niet verplicht)',
    'tooltip_instagram' => 'Vul een Instagram gebruikersnaam in. Wordt op de navigatie bar gezet (niet verplicht)',
    'tooltip_pinterest' => 'Vul een Pinterest gebruikersnaam in. Wordt op de navigatie bar gezet (niet verplicht)',
  ),
);
