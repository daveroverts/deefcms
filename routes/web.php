<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Homepage
Route::get('/', function () {
    return view('home');
});

Route::get('/lang/{lang}', function ($lang) {
    Session::put('locale', $lang);
    return back();
})->name('langroute');

// Routes related to accounts. Register, login, logout, and passwordReset
Auth::routes();

Route::get('/admin', function () {
    if (Auth::check() && Auth::user()->isContentCreator()){
        return view('admin.home');
    }
    else return redirect('login');
});

// Settings page
Route::get('/admin/settings', 'CoreController@showEditPage');
Route::post('/admin/settings', 'CoreController@editSettings')->name('editSettings');

// Change page online status
Route::get('/admin/page/{slug}/setOnline/{setOnline}', 'PageController@setOnline')->name('page.setOnline');

// Show page
Route::get('/{slug}', 'PageController@show')->name('page.show');

// All page routes, expect for show
Route::resource('/admin/page', 'PageController')->except('create','show');
// Create page, with optional headPage parameter
Route::get('/admin/page/create/{headPage?}', 'PageController@create')->name('page.create');
