<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('settings')->insert([
            'name' => 'name',
            'value' => 'DeefCMS',
            "created_at" => NOW(),
            "updated_at" => NOW(),
        ]);
        $settings = [
            ['name' => 'street', 'created_at' => NOW(), 'updated_at' => NOW()],
            ['name' => 'postal_code', 'created_at' => NOW(), 'updated_at' => NOW()],
            ['name' => 'city', 'created_at' => NOW(), 'updated_at' => NOW()],
            ['name' => 'telephone_number', 'created_at' => NOW(), 'updated_at' => NOW()],
            ['name' => 'facebook_link', 'created_at' => NOW(), 'updated_at' => NOW()],
            ['name' => 'twitter_link', 'created_at' => NOW(), 'updated_at' => NOW()],
            ['name' => 'youtube_link', 'created_at' => NOW(), 'updated_at' => NOW()],
            ['name' => 'google_link', 'created_at' => NOW(), 'updated_at' => NOW()],
            ['name' => 'instagram_link', 'created_at' => NOW(), 'updated_at' => NOW()],
            ['name' => 'pinterest_link', 'created_at' => NOW(), 'updated_at' => NOW()],
        ];
        DB::table('settings')->insert($settings);
    }
}
