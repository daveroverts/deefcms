<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            "name" => "Administrator",
            "username" => "admin",
            "email" => "dave@loyals.nl",
            "password" => bcrypt('admin'),
            "isContentCreator" => true,
            "isAdmin" => true,
            "created_at" => NOW(),
            "updated_at" => NOW(),
        ]);
    }
}
