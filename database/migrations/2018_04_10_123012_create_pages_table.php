<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pages', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('created_by')->default('1')->nullable();
            $table->string('title');
            $table->string('slug');
            $table->text('content');
            $table->boolean('isOnline')->default(true);
            $table->unsignedInteger('parentPageId')->nullable();
            $table->boolean('allowOnline')->default(true);
            $table->boolean('allowEdit')->default(true);
            $table->boolean('allowDelete')->default(true);
            $table->boolean('allowSub')->default(true);
            $table->boolean('allowLock')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pages');
    }
}
