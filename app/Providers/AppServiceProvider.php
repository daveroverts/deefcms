<?php

namespace App\Providers;

// Uncomment this when running MySQL 5.7.7 or older, MariaDB 10.2.2 or older, and don't want to enable innodb_large_prefix
//use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
//        Uncomment this when running MySQL 5.7.7 or older, MariaDB 10.2.2 or older, and don't want to enable innodb_large_prefix
//        Schema::defaultStringLength(191);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
