<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Page extends Model
{
    use Sluggable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'created_by', 'title', 'slug', 'content', 'parentPageId','allowOnline','allowEdit','allowDelete','allowSub','allowLock',
    ];

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ],
        ];
    }

    public function user()
    {
        return $this->hasMany(User::class);
    }

    public function headPage()
    {
        return $this->hasOne(Page::class, 'id', 'parentPageId');
    }

    public function hasSubPage()
    {
        if ($page = Page::where('parentPageId', $this->id)->get()) {
            if (!empty($page->toArray())) {
                return true;
            }
            return false;
        }
        return false;
    }

    public function isOnline()
    {
        if ($this->isOnline == 1) {
            return true;
        }
        return false;
    }

    public function allowOnline()
    {
        if ($this->allowOnline == 1) {
            return true;
        }
        return false;
    }

    public function allowEdit()
    {
        if ($this->allowEdit == 1) {
            return true;
        }
        return false;
    }

    public function allowDelete()
    {
        if ($this->allowDelete == 1) {
            return true;
        }
        return false;
    }

    public function allowSub()
    {
        if ($this->allowSub == 1) {
            return true;
        }
        return false;
    }

    public function allowLock()
    {
        if ($this->allowLock == 1) {
            return true;
        }
        return false;
    }
}
