<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use App\Setting;

class CoreController extends Controller
{
    // Function to show the edit page for the website
    public function showEditPage()
    {
        if (Auth::check() && Auth::user()->isAdmin()) {
            // Retrieve all settings, and give them all a name to make it easier to call the settings
            $settings = Setting::all()->mapWithKeys(function ($item) {
                return [$item['name'] => $item['value']];
            });
            return view('core.edit', compact('settings'));
        }
        else return redirect('/');
    }

    public function editSettings(Request $request)
    {
        $request->validate(['name' => 'required']);
        $settings = Setting::all();
        $settingsToChange = [
            'name', 'street', 'postal_code', 'city', 'telephone_number', 'facebook_link', 'twitter_link', 'youtube_link', 'google_link', 'instagram_link', 'pinterest_link',
        ];
        foreach ($settingsToChange as $value) {
            $setting = $settings->firstWhere('name', $value);
            $setting->value = $request->$value;
            $setting->save();
        }
        return redirect('/admin/settings');
    }
}
