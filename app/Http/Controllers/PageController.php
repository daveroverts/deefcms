<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Page;
use Illuminate\Support\Facades\Auth;


class PageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::check() && Auth::user()->isContentCreator()) {
            $pages = Page::all();
            $relations = $this->getPagesRelation();
            return view('page.overview', compact('pages', 'relations'));
        } else return redirect('/');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($headPage = null)
    {
        if (Auth::check() && Auth::user()->isContentCreator()) {
            if (isset($headPage)){
                $headPage = Page::whereKey($headPage)->first();
                if (empty($headPage) || !$headPage->allowSub()) {
                    $headPage = null;
                }
            }
            return view('page.create', compact('headPage'));
        } else return redirect('/');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'created_by' => 'bail:required:int',
            'title' => 'required:string|min:3',
            'tinymce' => 'sometimes:string|min:9',
            'isOnline' => 'required:boolean',
        ]);
        $page = Page::create([
            'title' => $request->title,
            'created_by' => Auth::user()->id,
            'content' => $request->tinymce,
        ]);
        if ($request->isOnline == 0) {
            $page->fill(['isOnline' => 0]);
        }
        if ($request->headPage != 0) {
            $page->fill(['parentPageId' => $request->headPage]);
        }

        $page->save();

        if ($request->headPage != 0) {
            $headPage = Page::find($request->headPage);
            $headPage->allowSub = 0;
            $headPage->save();
        }

        return redirect('/admin/page');
    }

    /**
     * Display the specified resource.
     *
     * @param  string $param
     * @return \Illuminate\Http\Response
     */
    public function show($param)
    {
        $page = Page::where('id', $param)
            ->orWhere('slug', $param)
            ->firstOrFail();
        if ($page->isOnline || Auth::check() && Auth::user()->isContentCreator()) {
            return view('page.show', compact('page'));
        } else return redirect('/page');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page = Page::findOrFail($id);
        if (Auth::check() && Auth::user()->isContentCreator()) {
            if ($page->allowEdit() || Auth::user()->isAdmin()){
                return view('page.edit', compact('page'));
            }
            else return back();
        }
        else return redirect('/');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'title' => 'required:string|min:3',
            'tinymce' => 'sometimes:string|min:9',
            'isOnline' => 'sometimes:boolean',
            'allowOnline' => 'sometimes|boolean',
            'allowEdit' => 'sometimes|boolean',
            'allowDelete' => 'sometimes|boolean',
            'allowSub' => 'sometimes|boolean',
            'allowLock' => 'sometimes|boolean',
        ]);
        $page = Page::find($id);
        $page->title = $request->title;
        $page->content = $request->tinymce;
        if (Auth::check() && Auth::user()->isAdmin()) {
            $allows = ['allowOnline','allowEdit','allowDelete','allowSub','allowLock',];
            foreach ($allows as $value) {
                $page->$value = $request->$value;
            }
        }

        // Checks if 'online changeable' is yes, or if current user is a administrator.
        if ($page->allowOnline() || Auth::check() && Auth::user()->isAdmin()){
            if ($request->isOnline) {
                $page->isOnline = $request->isOnline;
            }
        }

        // Checks if 'Lock URL Path' is no, then change slug
        if (!$page->allowLock()) {
            $page->slug = null;
        }

        $page->save();
        return redirect('/admin/page');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Get page by id
        $page = Page::find($id);
        // Check if page is allowed to be removed
        if ($page->allowDelete()) {
            // Check if page belongs to a head-page
            if ($page->parentPageId != 0) {
                $headPage = Page::find($page->parentPageId);
                // Allow head-page to have a sub-page again
                $headPage->allowSub = 1;
                $headPage->save();
            }
            // Delete the page
            $page->delete();
        }
        return redirect('/admin/page');
    }

    /**
     * Changes the online status for the specified resource from storage.
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function setOnline(Request $request, $id)
    {
        $page = Page::findOrFail($id);
        if ($request->setOnline == 0) {
            $page->isOnline = 1;
        } else $page->isOnline = 0;
        $page->save();
        return redirect('/admin/page');
    }

    /**
     * Function to get all pages that belong to other pages
     * @return \Illuminate\Support\Collection
     */
    public function getPagesRelation()
    {
        //Initialise collection where all pages with/without relation will be inserted
        $results = collect();

        //Run query to get all id's and parentPageId's
        $pages = Page::all();

        //Run this script while there are still pages
        $pages->each(function ($pageItem) use ($results, $pages) {

            //Run query to see if the current page has a parentPageId. If yes, put the results in a array.
            $isSub = Page::where('parentPageId', $pageItem['id'])->get();

            //Push only the current page id into the collection
            if (count($isSub) == 0) {
                if (empty($pageItem['parentPageId'])) {
                    $results->push([
                        'id' => $pageItem['id'],
                        'subPageId' => null
                    ]);
                }
            } else {
                //Push all relations of one page into the collection
                $isSub->each(function ($subPageItem) use ($results, $pageItem) {
                    $results->push([
                        'id' => $pageItem['id'],
                        'subPageId' => $subPageItem['id']
                    ]);
                });
            }
        });
        //Sort the collection by id's
        $results = $results->sortBy('id')->values()->all();
        return $results;
    }
}